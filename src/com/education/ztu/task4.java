package com.education.ztu;

import java.util.Locale;

public class task4 {
    public static void main(String[] args) {
        Check check = new Check();

        // �������� ��� �� ��������� ���
        Locale ukrainianLocale = new Locale("ua", "UA");
        check.printCheck(ukrainianLocale);

        // �������� ��� �� ��������� ���
        Locale englishLocale = new Locale("en", "US");
        check.printCheck(englishLocale);

        // �������� ��� �� ���������� ���
        Locale frenchLocale = new Locale("fr", "FR");
        check.printCheck(frenchLocale);
    }

}
